package com.dcits.controller;

import com.dcits.service.UserService;
import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;

public class LoginController extends Controller {

    private static final String FORM_ITEM_CODE = "validCode";
    @ActionKey("/")
    public void index(){
        // renderText("hello");
        render("/login.html");
    }

    //生成验证码
    public void getCaptcha(){
        renderCaptcha();
    }

    public void toLogin(){
        String result = "";
        //验证验证码
        if(validateCaptcha(FORM_ITEM_CODE)){


           /* UserService service = new UserService();
           service.deleteById(22);*/

            String UserName = getPara("username");

            setSessionAttr("SessionUser", UserName);

            // 验证码验证成功
            result = "{\"returnCode\":\"0\",\"msg\":\"登录成功\"}";

        }else{
            result = "{\"returnCode\":\"1\",\"msg\":\"验证码错误\"}";
        }

        renderJson(result);
    }

    public  Boolean IsLogin(){
        //判断是否登录
        if(getSessionAttr("SessionUser")==null){
            return false;
        }else{
            return true;
        }

    }
}
