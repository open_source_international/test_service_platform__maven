package com.dcits.service;

import com.dcits.model.User;
import com.jfinal.plugin.activerecord.Page;

public class UserService {
    private User dao =new User().dao();

    public Page<User> paginate(int pageNumber, int pageSize) {

        return dao.paginate(pageNumber, pageSize, "select * ","from tsp_user order by id asc");
    }

    public User findById(int id){
        return dao.findById(id);
    }
    public void deleteById(int id) {
        dao.deleteById(id);
    }

}
