package com.dcits.interceptor;



import com.jfinal.handler.Handler;
import com.jfinal.kit.HandlerKit;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.regex.Pattern;


//// *	 * 配置全局处理器
public class HtmlSkipHandler extends Handler {

    @SuppressWarnings("deprecation")
    public void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean[] isHandled) {

        //拦截除了登录相关和css、js、ico、npg等之外的所有请求,判断是否已经有登录的session
        if (Pattern.matches("/",target)||Pattern.matches("/login.*",target)||Pattern.matches("/error.*",target)){
            next.handle(target, request, response, isHandled);
        }else{
            if(Pattern.matches(".*\\.((?!html|\\d+).)*",target)){
                next.handle(target, request, response, isHandled);
            }else{
                //校验session，若不通过则跳转登录页
                if (request.getSession().getAttribute("SessionUser")==null){
                      //  response.sendError(404,"非法操作");
                        //response.sendRedirect("login.html");

                        HandlerKit.redirect("/login",request,response,isHandled);

                }else {
                    next.handle(target, request, response, isHandled);
                }

            }
        }


    }


}