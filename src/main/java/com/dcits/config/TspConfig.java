package com.dcits.config;

import com.dcits.controller.IndexController;
import com.dcits.controller.LoginController;
import com.dcits.interceptor.HtmlSkipHandler;
import com.dcits.model._MappingKit;
import com.jfinal.config.*;
import com.jfinal.ext.handler.ContextPathHandler;
import com.jfinal.ext.handler.UrlSkipHandler;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.template.Engine;



public class TspConfig extends JFinalConfig {

    static Prop p;

    /* *
     * 启动入口，运行此 main 方法可以启动项目，此 main 方法可以放置在任意的 Class 类定义中，不一定要放于此
     */
/*    public static void main(String[] args) {
        JFinal.start("webapp", 80, "/");

    }*/

    /* *
     * 初始化配置文件init.properties
     */
    static void loadConfig(){
        if (p == null){
            p = PropKit.use("init.properties");
        }
    }


    /**
     * 配置常量
     */
    @Override
    public void configConstant(Constants me) {
        me.setError404View("/error.html");
        loadConfig();

        me.setDevMode(p.getBoolean("devMode", false));
        // 支持 Controller、Interceptor 之中使用 @Inject 注入业务层，并且自动实现 AOP
        me.setInjectDependency(true);
    }

    /**
     * 配置路由
     */
    @Override
    public void configRoute(Routes me) {
        me.setBaseViewPath("/");
        //路由拦截器
       // me.addInterceptor(new RouteInterceptor());
        me.add("/login",LoginController.class,"/");
        me.add("/index",IndexController.class,"/");
      //  me.addInterceptor()
    }


    @Override
    public void configEngine(Engine me) {

    }

    /**
     * 配置插件
     */
    @Override
    public void configPlugin(Plugins me) {
        // 配置 druid 数据库连接池插件
        DruidPlugin druidPlugin =createDruidPlugin();
        druidPlugin.set(p.getInt("db.initialSize"), p.getInt("db.minIdle"),
                p.getInt("db.maxActive"));
        me.add(druidPlugin);

        // 配置ActiveRecord插件
        ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin);
        _MappingKit.mapping(arp);
        me.add(arp);
    }

    public static DruidPlugin createDruidPlugin(){
        loadConfig();
       // System.out.println(p.get("db.jdbcUrl")+p.get("db.user")+p.get("db.password"));
        return new DruidPlugin(p.get("db.jdbcUrl"),p.get("db.user").trim(),p.get("db.password").trim(),p.get("db.driverClass"));
    }

    /**
     * 配置全局拦截器
     */
    @Override
    public void configInterceptor(Interceptors me) {

    }

    /**
     * 配置处理器
     */
    @Override
    public void configHandler(Handlers me) {
        me.add(new HtmlSkipHandler());
       //根路径
        //me.add(new ContextPathHandler("rptah"));
        me.add(new UrlSkipHandler(".*html",false));



    }



}
