/**
 * 自定义layui扩展模块
 */
layui.config({
    base: '/rmp/js/ext/'
});

/**
 * 请求url
 */
var VALIDATE_USER_KEY_URL = '/rmp/mock/login.json';
var SERVER_INFO_LIST_ALL_URL = '../mock/listAllServerInfo.json';
var SERVER_INFO_LIST_PAGE_URL = '../mock/pageListServerInfo.json';
var SERVER_INFO_DEL_URL = '../mock/delServer.json';
var SERVER_INFO_EDIT_URL = '../mock/editServer.json';

/**
 * 提前定义好不同类型服务器信息<br>
 * 通过该配置使用JS渲染页面
 */
var serverInfos = {
    linux:{
        serverInfoTableTypeColor:"orange"
    },
    weblogic:{
        serverInfoTableTypeColor:"blue"
    },
    jvm:{

    }
};
var userKey = top.userKey;


/*********************************全局公共方法*******************************************/
/**
 * 验证返回值
 * @param json
 * @param callback
 */
function validatReturnJson(json, successCallback){
    if (json.returnCode == 0) {
        successCallback != null && successCallback();
    } else {
        layer.alert(json.msg == null ? "系统错误,请重试!" : json.msg, {icon:5});
    }
}

/**
 * 验证userKey
 * @param userKey
 */
function validateUserKey(userKey, hrefFlag) {
    $.post(VALIDATE_USER_KEY_URL, {userKey:userKey}, function(json){
        validatReturnJson(json, function(){
            if (hrefFlag) {
                window.location.href = '../index.html';
            }
            layui.data('rmp', {
                key:'userKey',
                value:json.userKey
            });
        })
    });
}


/**
 * 批量操作方法
 * @param url   url 远程操作接口
 * @param tableId   layui table id
 * @param opName 操作方式名称 默认为 删除
 * @param idName 对应实体的ID名称
 * @param textName 名称的属性名
 * @param otherSendData 其他要随着ID发送的参数
 * @returns {Boolean}
 */
function batchOp (url, opName, tableId, idName, textName, otherSendData, callback) {
    var checkdata =  layui.table.checkStatus(tableId).data;
    if (checkdata.length < 1) {
        return false;
    }

    if (opName == null) {
        opName = "删除";
    }
    layer.confirm('确认' + opName + '选中的' + checkdata.length + '条记录?', {icon:0, title:'警告'}, function(index) {
        layer.close(index);
        var loadindex = layer.msg('正在进行批量' + opName + "...", {icon:16, time:60000, shade:0.35});
        var delCount = 0;
        var totalCount = 0;
        var errorTip = "";
        $.each(checkdata ,function(i, n) {
            var objId = n[idName];//获取id
            var objName = n[textName];	//name属性为对象的名称
            var params = {id: objId};
            params[idName] = objId;
            if (otherSendData != null && otherSendData instanceof Object) {
                $.each(otherSendData, function(i, n) {
                    params[i] = n;
                });
            }

            //layer.msg("正在" + opName + objName + "...", {time: 999999});
            $.ajax({
                type:"post",
                url:url,
                data:params,
                //async:false,
                success:function(data) {
                    totalCount++;
                    if(data.returnCode != 0) {
                        errorTip += "[" + objName + "]<br>";
                    }else{
                        delCount++;
                    }
                }
            });
        });

        var intervalID = setInterval(function() {
            if (totalCount == checkdata.length) {
                clearInterval(intervalID);
                if (callback != null) {
                    callback();
                }
                layui.table.reload(tableId, {
                    done:function(){
                        layer.close(loadindex);
                        if (errorTip != "") {
                            errorTip = "在" + opName + "<br>" + errorTip + "时发生了错误<br>请执行单笔" + opName + "操作!";
                            layer.alert(errorTip, {icon:5}, function(index) {
                                layer.close(index);
                                layer.msg("共" + opName + delCount + "条数据!", {icon:1, time:2000});
                            });
                        } else {
                            layer.msg("共" + opName + delCount + "条数据!", {icon:1, time:2000});
                        }
                    }
                });
            }
        }, 500);
    });
}

/**
 * 批量以代理的方式绑定监听事件
 * @param configs
 * @returns {$}
 */
$.fn.delegates = function(configs) {
    el = $(this[0]);
    for (var name in configs) {
        var value = configs[name];
        if (typeof value == 'function') {
            var obj = {};
            obj.click = value;
            value = obj;
        };
        for (var type in value) {
            el.delegate(name, type, value[type]);
        }
    }
    return this;
};

/**
 * 快速创建layer弹出层
 * 参数解释
 * @param title 标题
 * @param url  url地址或者html页面
 * @param w 宽度,默认值
 * @param h 高度,默认值
 * @param type 类型 1-页面层 2-frame层
 * @param success 成功打开之后的回调函数
 * @param cancel 右上角关闭层的回调函数
 * @param end 层销毁之后的回调
 * @param other 其他DT设置
 * @returns index
 */
function layer_show (title, url, w, h, type, success, cancel, end, other) {
    var maxHeight = $(document).height();
    var maxWidth = $(document).width();

    if (other == null) {
        other = {};
    }

    if (title == null || title == '') {
        title = false;
    };
    if (url == null || url == '') {
        url="404.html";
    };
    if (w == null || w == '' || w >= maxWidth) {//设置最大宽度
        w =	maxWidth * 0.9
    };
    if (h == null || h == '' || h >= maxHeight) {//设置最大高度
        h= (maxHeight * 0.86) ;
    };
    if (type == null || type == '') {
        type = 2;
    }
    index = layer.open($.extend(true, {
        type: type,
        area: [w + 'px', h + 'px'],
        fix: false, //不固定
        maxmin: false,
        shade:0.4,
        anim:5,
        shadeClose:true,
        title: title,
        content: url,
        success:success,
        cancel:cancel,
        end:end
    } ,other));
    return index;
}
