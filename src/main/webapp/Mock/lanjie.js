/**Create on 2018/2/27.
 *作者：戈鹏飞
 *功能：
 */
//在nodejs下单独运行加require
// var Mock = require('mockjs');
////mockjs拦截
var mockurl = 'http://127.0.0.1:3000';
/* var  mockdata = Mock.mock(mockurl+'/user-toLogin', {
    'returnCode|0': 0,
    'msg': '11登录成功',

});*/
var  mockdata2 = Mock.mock({
    'returnCode|2': 1,
    'data': '11登录成功'

});
 Mock.mock(mockurl+'/get-listAllServerInfo?page=1&limit=10',{
    "code": 0,
    "msg|1-2": "1",
    "count": 100,
    'data|50-100':[
        {
            "id|+1": 10000,
            "host": '@ip',
            "port|21-9999":22,
            "realHost": '@ip',
            "username": '@cname',
            "password": '@word(5)',
            "serverType": "linux",
            "createTime": '@date @time',
            "lastUseTime": '@date @time',
            "tags": '@name @integer(60, 100) @string("lower", 5)',
            "mark": '@string("lower", 5)',
            "parameters": "{\"javaHome\":\"/opt/java/bin\"}"
        }
    ]


});

